# Will install & configure PowerShell packages
# This code is idempotent

if ($PSVersionTable.PSVersion.Major -lt 5) {
    Write-Warning "PowerShell needs upgrading"
    if (!(Test-Path "$($env:ProgramData)\chocolatey\choco.exe")) {
        & choco install powershell
    }
}

# Cr�er un fichier profil
if (Test-Path $profile) {
    Write-Output "$profile already exists"
} else {
    New-Item $profile -ItemType File -Force
}

# S'assurer que le home est bien C:\User\Username
if ($home -notmatch "^C:\\Users\\$($env:username)") {
    Write-Warning '$HOME must be changed in $profile'
    return
}

# Cr�er un dossier Code sous $home
if (!(Test-Path "$home\Code")) {
    $null = New-Item "$home\Code" -ItemType Directory
}
