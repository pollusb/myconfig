# Will install and configure choco
# This code is idempotent. It will work even if choco is already installed

if (Test-Path "$($env:ProgramData)\chocolatey\choco.exe") {
    Write-Output "Choco is already installed"
} else {
    Set-ExecutionPolicy Bypass -Scope Process -Force
    Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    return
}
& choco feature enable -n allowGlobalConfirmation 
& choco feature enable -n useRememberedArgumentsForUpgrades
& choco upgrade all

